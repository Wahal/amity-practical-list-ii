#!/usr/bin/env python

print """
#--------------------------------------------------#
# Project : Amity Practical List Class XII         #
# Language: Python                                 #
# Script  : Question 8                             #
# Author  : Mrinal Wahal                           #
#--------------------------------------------------#
"""

import os

class Polygon:
    
    sides = []
    
    def __init__(selfie,x): selfie.n = x
        
    def inputsides(selfie):
        for sherlock in range(selfie.n): Polygon.sides.append(input("Enter Sides For Polygon: "))
        
    def display_sides(selfie): return Polygon.sides
    
class Triangle(Polygon):
    
    def __init__(selfie): selfie.s = 3
    
    def is_triangle(selfie):
        
        if len(Polygon.sides) == selfie.s:
            if (Polygon.sides[0] > Polygon.sides[1] + Polygon.sides[2]) or (Polygon.sides[1] > Polygon.sides[0] + Polygon.sides[2]) or (Polygon.sides[2] > Polygon.sides[0] + Polygon.sides[1]):
                print "A Triangle Isn't Possible."
            elif (Polygon.sides[0] == Polygon.sides[1] + Polygon.sides[2]) or (Polygon.sides[1] == Polygon.sides[0] + Polygon.sides[2]) or (Polygon.sides[2] == Polygon.sides[0] + Polygon.sides[1]):
                print 'A Triangle Is Possible.'
                print "Area: ", area(selfie)
            else:
                print 'A Triangle Is Possible.'
                print "Area: ", area(selfie)
        else: print "A Triangle Isn't Possible."
            
    global area
    def area(selfie):
        
        semi = (Polygon.sides[0] + Polygon.sides[1] + Polygon.sides[2]) / 2
        return (semi*(semi - Polygon.sides[0])*(semi - Polygon.sides[1])*(semi - Polygon.sides[2]))**(0.5)

def main():
    
    try:
        
        sides_of_polygon = input("Enter The Number Of Sides For The Polygon: ")
        beta = Polygon(sides_of_polygon)
        beta.inputsides()
        print "Sides Of The Polygon: ", beta.display_sides()
        alpha = Triangle()
        alpha.is_triangle()
        
    except Exception, e: print "Error: " + str(e)
    
    finally:
        
        print
        os.system("pause")
        
if __name__ == '__main__': main()
    
    