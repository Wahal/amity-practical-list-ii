#!/usr/bin/env python

print """
#--------------------------------------------------#
# Project : Amity Practical List Class XII         #
# Language: Python                                 #
# Script  : Question 2                             #
# Author  : Mrinal Wahal                           #
#--------------------------------------------------#
"""

import os

class Numeric:
    
    def __init__(selfie): selfie.n = 1
    
    def getdata(selfie):
        
        try:
            selfie.n = input("Enter Your Desired Value: ")
        except Exception, e: print "Error: " + str(e)
    
    def count(selfie): print len(str(selfie.n))
    
    def show(selfie):
        
        global empty
        empty = [x for x in str(selfie.n)]
        nonempty = []
        zero = 0
        
        for element in empty:
            zero += 1
            nonempty.append(int(element + ("0"*(len(empty) - zero))))
        
        for element in nonempty: print element, " + ",
        print "0 = ", sum(nonempty)
        
    
    def reverse(selfie):
        
        string = ""
        for element in empty[::-1]: string += str(element)
        return string
    
    def has_digit(selfie,x):
        
        if str(x) in str(selfie.n): print "The Number Incorporates X."
        else: print "The Number Doesn't Incorporates X."
        
def main():
    
    try:
        alpha = Numeric()
        alpha.getdata()
        alpha.count()
        alpha.show()
        print "The Reverse Number Is: ", alpha.reverse()
        
        x = input("Enter A Number To Check: ")
        alpha.has_digit(x)
        
    except Exception, e: print "Error: " + str(e)
    
    finally:
        
        print
        os.system("pause")

if __name__ == '__main__': main()
    