#!/usr/bin/env python

print """
#--------------------------------------------------#
# Project : Amity Practical List Class XII         #
# Language: Python                                 #
# Script  : Question 7                             #
# Author  : Mrinal Wahal                           #
#--------------------------------------------------#
"""

import os

global minform

class Rational:
    
    def __init__(gamma, n, d = 1):
        
        gamma.n = n
        gamma.d = d
        
    __sum__ = lambda gamma, n2, d2: (gamma.n/gamma.d) + (n2/d2)
        
    __sub__ = lambda gamma, n2, d2: (gamma.n/gamma.d) - (n2/d2)
        
    __mul__ = lambda gamma, n2, d2: (gamma.n/gamma.d) * (n2/d2)
        
    __div__ = lambda gamma, n2, d2: (gamma.n//gamma.d) / (n2//d2)
    
    global minform    
    def minform(gamma, num, den):
        
        if num % den == 0: return den
        else: return minform(gamma, den, num % den)
    
    def simple_form(gamma):
        
        num = gamma.n / minform(gamma, gamma.n, gamma.d)
        den = gamma.d / minform(gamma, gamma.n, gamma.d)
        
        print "Simplest Form Of Fraction One: ", num, "/", den
    
    def __str__(gamma): print "Fraction One: " + str(gamma.n) + "/" + str(gamma.d)
    
def main():
    
    try:
        n = input("Enter First Numerator: ")
        d = input("Enter First Denominator: ")
        n2 = input("\nEnter Second Numerator: ")
        d2 = input("Enter Second Denominator: ")
        
        alpha = Rational(n,d)
        print "Sum of Fractions: ", alpha.__sum__(n2,d2)
        print "Difference In Fractions: ", alpha.__sub__(n2,d2)
        print "Product Of Fractions: ", alpha.__mul__(n2,d2)
        print "Division Of Fractions: ", alpha.__div__(n2,d2)
        
        alpha.simple_form()
        alpha.__str__()
        
    except Exception, e: print "Error: " + str(e)    
    
    finally:
        print
        os.system("pause")
    
if __name__ == '__main__': main()
    
    