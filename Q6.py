#!/usr/bin/env python

print """
#--------------------------------------------------#
# Project : Amity Practical List Class XII         #
# Language: Python                                 #
# Script  : Question 6                             #
# Author  : Mrinal Wahal                           #
#--------------------------------------------------#
"""

import os, random

class Matrix:
    
    def __init__(selfie,x,y):
        
        selfie.x = x
        selfie.y = y
        selfie.a = [[random.random() for row in range(selfie.x)] for col in range(selfie.y)]
        
    def generate(selfie):
        
        zero = 0
        print
        
        for x in range(selfie.x):
            for y in range(selfie.y):
                zero += 1
                selfie.a[x][y] = input("Enter Element %d: " % zero)
                
    def display(selfie):
        
        for x in range(selfie.x):
            for y in range(selfie.y):
                print selfie.a[x][y], "\t",
            print
            
    def diagonal(selfie):
        
        if selfie.x == selfie.y:
            
            empty = []
            for x in range(selfie.x):
                empty.append(selfie.a[x][x])
            
            print "\nSum Of Diagonal One: ", sum(empty)
            
            z = selfie.x - 1
            
            nonempty = []
            for y in range(selfie.y):
                nonempty.append(selfie.a[y][z])
                z -= 1
            
            print "Sum of Diagonal Two: ", sum(nonempty)
            print "Sum Of Both Diagonals: ", sum(empty) + sum(nonempty)
        
        else: print "Diagonal Sum Not Possible."
        
    def rows_more(selfie,num):
        
        for row in selfie.a:
            if sum(row) > num: print row,
            
def main():
    
    try:
        rows = input("Enter The Numbers Of Rows: ")
        cols = input("Enter The Number Of Columns: ")
        
        if rows == cols: 
            alpha = Matrix(rows,cols)
            alpha.generate()
            
            print "\nThe Matrix Is As Followed: \n"
            alpha.display()
            alpha.diagonal()
            
            num = input("\nEnter A Number To Check: ")
            print "\nRows Are As Follwed: ",  
            alpha.rows_more(num)
        
        else: print "Error: Matrix Not Possible."
        
    except Exception, e: print "Error: " + str(e)    
    
    finally:
        print
        os.system("pause")
    
if __name__ == '__main__': main()