#!/usr/bin/env python

print """
#--------------------------------------------------#
# Project : Amity Practical List Class XII         #
# Language: Python                                 #
# Script  : Question 12                            #
# Author  : Mrinal Wahal                           #
#--------------------------------------------------#
"""

import os, random

global face_cards
face_cards = ["Jack","Queen","King"]

class Card:
    
    def __init__(selfie):
        
        selfie.ranks = ["Ace","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Jack","Queen","King"]
        selfie.suits = ["Spades","Clubs","Hearts","Diamonds"]
        
        selfie.rank = random.choice(selfie.ranks)
        selfie.suit = random.choice(selfie.suits)
        
    def get_rank(selfie): return selfie.rank
    
    def get_suit(selfie): return selfie.suit
    
    def comp(selfie,card):

        if selfie.suit == card[1]: return True
        else: return False
        
    def rank2num(selfie): return (selfie.ranks.index(selfie.rank) + 1)
        
    def isFaceCard(selfie):
        
        constant = False
        for card in face_cards:
            if selfie.rank == card: constant = True
        
        if constant == False: return False
        else: return True
        
    def sumcard(selfie,card2):
        
        score = 0
        
        ranks_dict = {}
        count = 0
        for rank in selfie.ranks:
            count += 1
            ranks_dict[rank] = count
        
        for card in face_cards:    
            ranks_dict[card] = 10
            
        score = ranks_dict[card2[0]]
        
        if selfie.suit == card2[1]: score += 100
        
        for card in face_cards:
            if selfie.rank == card and card2[0] == card: scroe += 50
        
        return score
    
def main():
    
    try:
        print "-"*30
        print "Player 1"
        print "-"*30
        player1_input = raw_input("Enter Rank,Suit: ")
        player1 = player1_input.split(",")
        
        print "-"*30
        print "Player 2"
        print "-"*30
        player2_input = raw_input("Enter Rank,Suit: ")
        player2 = player2_input.split(",")
        
        alpha = Card()
        if alpha.comp(player1) == True: print "Player 1 Got The Correct Suit."
        else: print "Player 1 Did Not Get The Correct Suit."
        if alpha.comp(player2) == True: print "Player 2 Got The Correct Suit."
        else: print "Player 2 Did Not Get The Correct Suit."
        
        print "Dealer's Rank: ", alpha.rank2num()
        if alpha.isFaceCard() == True: print "Dealer Has a Face Card."
        
        score_of_player_1 = alpha.sumcard(player1)
        score_of_player_2 = alpha.sumcard(player2)
        print "Score Of Player 1: ", score_of_player_1
        print "Score Of Player 2: ", score_of_player_2
        
        if score_of_player_1 > score_of_player_2: print "Player 1 Wins !"
        else: print "Player 2 Wins !"
        
    except Exception, e: print "Error: " + str(e)
    
    finally:
        
        print
        os.system("pause")
        
if __name__ == '__main__': main()
    